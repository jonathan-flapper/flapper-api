package com.flapper.api.domain

enum class Location {
  LONDON,
  PARIS,
  SINGAPORE,
  NEW_YORK,
  SYDNEY,

  ONLINE,
}
