package com.flapper.api.routes

import com.flapper.api.domain.Location
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBodyList

@ExtendWith(MockitoExtension::class)
class LocationHandlerTest {

  private val handler = LocationHandler()
  private val router = Routes(mockk(), mockk(), handler)

  private val client = WebTestClient.bindToRouterFunction(router.locations()).build()

  @Test
  fun whenGetAllLocations_shouldReturnCategories() {
    client.get().uri("/locations")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Location>()
      .contains(*Location.values())
  }
}
