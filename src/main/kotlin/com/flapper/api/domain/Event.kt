package com.flapper.api.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import java.time.LocalDate

@Document
data class Event(
  @Id
  val id: String? = null,
  val name: String,
  val description: String,

  val date: LocalDate,
  val category: Category,
  val location: Location = Location.ONLINE,
)

@Repository
interface EventRepository : ReactiveMongoRepository<Event, String> {
  fun findAllByCategoryInAndLocationAndDateAfter(categories: List<Category>, location: Location, date: LocalDate): Flux<Event>
}
