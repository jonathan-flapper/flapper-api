package com.flapper.api.domain

enum class Category {
  ART,
  CHARITY,
  CONCERT,
  FAMILY,
  SPORT,
  TECHNOLOGY,
}
