package com.flapper.api.routes

import com.flapper.api.domain.Category
import com.flapper.api.domain.Event
import com.flapper.api.domain.EventRepository
import com.flapper.api.random
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import org.springframework.test.web.reactive.server.expectBodyList
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(MockitoExtension::class)
internal class EventHandlerTest {

  private val eventRepository = mockk<EventRepository>()
  private val handler = EventHandler(eventRepository)
  private val router = Routes(mockk(), handler, mockk())

  private val client = WebTestClient.bindToRouterFunction(router.events()).build()

  @Test
  fun whenGetAll_shouldReturnAll() {
    val event1 = random.nextObject(Event::class.java)
    val event2 = random.nextObject(Event::class.java)
    val event3 = random.nextObject(Event::class.java)
    val event4 = random.nextObject(Event::class.java)
    val event5 = random.nextObject(Event::class.java)
    every { eventRepository.findAll() } returns Flux.just(event1, event2, event3, event4, event5)

    client.get().uri("/events")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Event>()
      .hasSize(5)
      .contains(event1, event2, event3, event4, event5)
  }

  @Test
  fun whenGetById_shouldReturnOne() {
    val event = random.nextObject(Event::class.java)
    every { eventRepository.findById(event.id!!) } returns Mono.just(event)

    client.get().uri("/events/{id}", event.id!!)
      .exchange()
      .expectStatus().isOk
      .expectBody<Event>()
      .isEqualTo(event)
  }

  @Test
  fun whenSearchWithCategories_andFound_shouldReturnResult() {
    val event1 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event2 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event3 = random.nextObject(Event::class.java).copy(category = Category.CONCERT)
    val event4 = random.nextObject(Event::class.java).copy(category = Category.CHARITY)
    val event5 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event6 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event7 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event8 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event9 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    every { eventRepository.findAllByCategoryInAndLocationAndDateAfter(any(), any(), any()) } answers {
      Flux //let's just filter for category since date and location are randomized here
        .just(event1, event2, event3, event4, event5, event6, event7, event8, event9)
        .filter { it.category in firstArg<List<Category>>() }
    }

    client.get().uri("/events/q?topics={topics}", "art,technology")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Event>()
      .contains(event1, event2, event7, event8, event9)

    client.get().uri("/events/q?topics={topics}", "concert,sport")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Event>()
      .contains(event3, event5, event6)

    client.get().uri("/events/q?topics={topics}", "technology")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Event>()
      .contains(event7, event8, event9)
  }

  @Test
  fun whenSearchWithCategories_andNotFound_shouldReturnResult() {
    val event1 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event2 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event3 = random.nextObject(Event::class.java).copy(category = Category.CONCERT)
    val event4 = random.nextObject(Event::class.java).copy(category = Category.CHARITY)
    val event5 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event6 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event7 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event8 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event9 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    every { eventRepository.findAllByCategoryInAndLocationAndDateAfter(any(), any(), any()) } answers {
      Flux //let's just filter for category since date and location are randomized here
        .just(event1, event2, event3, event4, event5, event6, event7, event8, event9)
        .filter { it.category in firstArg<List<Category>>() }
    }

    client.get().uri("/events/q?topics={topics}", "family")
      .exchange()
      .expectStatus().isNotFound
      .expectBodyList<Event>()
      .hasSize(0)
  }

  @Test
  fun whenSearchWithoutCategory_shouldReturnAll() {
    val event1 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event2 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event3 = random.nextObject(Event::class.java).copy(category = Category.CONCERT)
    val event4 = random.nextObject(Event::class.java).copy(category = Category.CHARITY)
    val event5 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event6 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event7 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event8 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event9 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    every { eventRepository.findAllByCategoryInAndLocationAndDateAfter(any(), any(), any()) } answers {
      Flux //let's just filter for category since date and location are randomized here
        .just(event1, event2, event3, event4, event5, event6, event7, event8, event9)
        .filter { it.category in firstArg<List<Category>>() }
    }

    client.get().uri("/events/q")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Event>()
      .contains(event1, event2, event3, event4, event5, event6, event7, event8, event9)
  }

  @Test
  fun whenSearchWithUnrecognizedCategory_shouldReturnAll() {
    val event1 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event2 = random.nextObject(Event::class.java).copy(category = Category.ART)
    val event3 = random.nextObject(Event::class.java).copy(category = Category.CONCERT)
    val event4 = random.nextObject(Event::class.java).copy(category = Category.CHARITY)
    val event5 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event6 = random.nextObject(Event::class.java).copy(category = Category.SPORT)
    val event7 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event8 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    val event9 = random.nextObject(Event::class.java).copy(category = Category.TECHNOLOGY)
    every { eventRepository.findAllByCategoryInAndLocationAndDateAfter(any(), any(), any()) } answers {
      Flux //let's just filter for category since date and location are randomized here
        .just(event1, event2, event3, event4, event5, event6, event7, event8, event9)
        .filter { it.category in firstArg<List<Category>>() }
    }

    client.get().uri("/events/q?topics={topics}", "something")
      .exchange()
      .expectStatus().isNotFound
      .expectBody().isEmpty
  }
}
