package com.flapper.api.routes

import com.flapper.api.domain.Category
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.json
import reactor.core.publisher.Mono

@Component
class CategoryHandler {

  fun getAll(request: ServerRequest): Mono<ServerResponse> {
    return Category.values().toList()
      .let { ServerResponse.ok().json().bodyValue(it) }
  }
}
