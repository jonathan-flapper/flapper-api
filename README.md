# Flapper API

This module serves as a backend to Flapper Application.

## Prerequisite

The module requires the following to run:

* Java (1.8+)
* Maven (3.6+)
* ... and working internet connection

## How to Run

### Running the application

Note:
* Active internet connection is required
* Maven test is implied

#### 1. Maven

```text
$ mvn spring-boot:run
```

#### 2. Java

```text
$ mvn clean package
$ java -jar target/flapper-api.jar
```

### Testing the application

Note:
* Tests are isolated


```text
$ mvn test
```
