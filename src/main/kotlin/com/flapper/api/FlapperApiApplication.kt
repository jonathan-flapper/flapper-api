package com.flapper.api

import com.flapper.api.domain.Category
import com.flapper.api.domain.Event
import com.flapper.api.domain.EventRepository
import com.flapper.api.domain.Location
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.web.cors.reactive.CorsWebFilter
import reactor.core.publisher.Flux
import java.time.LocalDate
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource


@Configuration
@SpringBootApplication
class FlapperApiApplication(
  private val eventRepository: EventRepository,
) {

  companion object {
    private val logger = LoggerFactory.getLogger(FlapperApiApplication::class.java)
  }

  @Bean
  fun corsWebFilter(): CorsWebFilter {
    return CorsConfiguration()
      .apply {
        allowedOrigins = listOf("*")
        allowedMethods = listOf("*")
        allowedHeaders = listOf("*")
        maxAge = 8000
      }
      .let { config -> UrlBasedCorsConfigurationSource().also { it.registerCorsConfiguration("/**", config) } }
      .let { CorsWebFilter(it) }
  }

  @EventListener(ApplicationReadyEvent::class)
  fun onApplicationReadyEvent(event: ApplicationReadyEvent) {
    logger.info(">> ready!")

    eventRepository.deleteAll()
      .thenMany(Flux.just(
        Event("technology-001", "KotlinConf 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 3, 7), Category.TECHNOLOGY),
        Event("technology-002", "Spring Summit 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 3, 8), Category.TECHNOLOGY),
        Event("technology-003", "ScalaCon 2021","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 3, 5), Category.TECHNOLOGY),
        Event("technology-004", "JS Conf 2021","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 3, 17), Category.TECHNOLOGY),

        Event("concert-001", "Eagles - Hell Freezes Over","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 6, 17), Category.CONCERT),
        Event("concert-002", "Pink Floyd - Reunion","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 4, 30 ), Category.CONCERT),
        Event("concert-003", "The Beatles - From The Grave","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 9, 18), Category.CONCERT),

        Event("sport-001", "Man Utd vs. Man City","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 9, 18), Category.SPORT),
        Event("sport-002", "Real Madrid vs. Barcelona","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 10, 18), Category.SPORT),
        Event("sport-003", "Bayern Munich vs. Borussia Dortmund","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 7, 18), Category.SPORT),

        Event("family-001", "Dads For Life","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", LocalDate.of(2021, 5, 18), Category.SPORT),
      ))
      .flatMap { eventRepository.save(it) }
      .subscribe()
  }
}

fun main(args: Array<String>) {
  runApplication<FlapperApiApplication>(*args)
}

fun String.asCategory(): Category? = Category.values().find { this.equals(it.name, true) }
fun String.asLocation(): Location? = Location.values().find { this.equals(it.name, true) }
