package com.flapper.api

import org.jeasy.random.EasyRandom
import org.jeasy.random.EasyRandomParameters
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDate

@SpringBootTest
class FlapperApiApplicationTests {

  @Test
  fun contextLoads() {
  }
}

val random = EasyRandom(
  EasyRandomParameters()
    .dateRange(LocalDate.now(), LocalDate.of(9999, 12, 31))
)
