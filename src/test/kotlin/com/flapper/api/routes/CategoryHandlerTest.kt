package com.flapper.api.routes

import com.flapper.api.domain.Category
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBodyList

@ExtendWith(MockitoExtension::class)
class CategoryHandlerTest {

  private val handler = CategoryHandler()
  private val router = Routes(handler, mockk(), mockk())

  private val client = WebTestClient.bindToRouterFunction(router.categories()).build()

  @Test
  fun whenGetAllCategories_shouldReturnCategories() {
    client.get().uri("/categories")
      .exchange()
      .expectStatus().isOk
      .expectBodyList<Category>()
      .contains(*Category.values())
  }
}
