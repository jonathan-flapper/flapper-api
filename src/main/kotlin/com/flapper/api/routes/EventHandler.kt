package com.flapper.api.routes

import com.flapper.api.asCategory
import com.flapper.api.asLocation
import com.flapper.api.domain.Category
import com.flapper.api.domain.EventRepository
import com.flapper.api.domain.Location
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.notFound
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.json
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono
import java.time.LocalDate

@Component
class EventHandler(private val eventRepository: EventRepository) {

  fun getAll(request: ServerRequest): Mono<ServerResponse> {
    return eventRepository.findAll()
      .collectList()
      .flatMap { ok().json().bodyValue(it) }
      .switchIfEmpty { notFound().build() }
      .onErrorResume { internalServerError().build() }
  }

  fun getById(request: ServerRequest): Mono<ServerResponse> {
    return request.pathVariable("id").toMono()
      .flatMap { eventRepository.findById(it) }
      .flatMap { ok().json().bodyValue(it) }
      .switchIfEmpty { notFound().build() }
      .onErrorResume { internalServerError().build() }
  }

  fun search(request: ServerRequest): Mono<ServerResponse> {
    val categories = request.queryParam("topics")
      .map { q -> q.split(",").mapNotNull { it.asCategory() } }
      .orElse(Category.values().toList())

    val location = request.queryParam("location")
      .map { q -> q.asLocation() }
      .orElse(Location.ONLINE)!!

    val date = request.queryParam("date")
      .map { LocalDate.parse(it) }
      .orElseGet { LocalDate.now() }

    return eventRepository.findAllByCategoryInAndLocationAndDateAfter(categories, location, date)
      .collectList()
      .flatMap {
        when (it.size > 0) {
          true -> ok().json().bodyValue(it)
          else -> notFound().build()
        }
      }
      .onErrorResume { internalServerError().build() }
  }
}

fun internalServerError(): ServerResponse.HeadersBuilder<*> = ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
