package com.flapper.api.routes

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.json
import org.springframework.web.reactive.function.server.router

@Configuration
class Routes(
  private val categoryHandler: CategoryHandler,
  private val eventHandler: EventHandler,
  private val locationHandler: LocationHandler,
) {

  @Bean
  fun hello() = router {
    GET("/hello") { ok().json().bodyValue(mapOf("hello" to "world")) }
  }

  @Bean
  fun categories() = router {
    GET("/categories", categoryHandler::getAll)
  }

  @Bean
  fun events() = router {
    "/events".nest {
      GET("", eventHandler::getAll)
      GET("/q", eventHandler::search)
      GET("/{id}", eventHandler::getById)
    }
  }

  @Bean
  fun locations() = router {
    GET("/locations", locationHandler::getAll)
  }
}
